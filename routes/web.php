<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PostController@indexFormularioPost')->name('indexFormularioPost');

Route::get('/indexFormularioPost', 'PostController@indexFormularioPost')->name('indexFormularioPost');
Route::get('/crearFormularioPost', 'PostController@crearFormularioPost')->name('crearFormularioPost');
Route::post('/storeFormularioPost', 'PostController@storeFormularioPost')->name('storeFormularioPost');




Auth::routes();


