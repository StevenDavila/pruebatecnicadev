	@csrf 
		<div class="form-group">
    		<div class="row">
            	<div class="col-xs-12 col-md-3">
            		<p style="text-align: center"><label for="nombre">Nombre Completo *</label></p>
            	</div>
				<div class="col-xs-12 col-md-9">
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<input class="form-control" type="text" name="nombre" id="nombre" pattern="^[a-z]">

							@error('nombre')
				    			<small class="text-danger">{{ $message }}</small>
				    		@enderror

						</div>
					</div>
                	
            	</div>
            </div>
		</div>

		<div class="form-group">
    		<div class="row">
            	<div class="col-xs-12 col-md-3">
            		<p style="text-align: center"><label for="email">Correo Electronico *</label></p>
            	</div>
				<div class="col-xs-12 col-md-9">
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<input class="form-control" type="text" name="email" id="email">
						</div>
					</div>
                	
            	</div>
            </div>
		</div>

		<div class="form-group">
		    
			
    		<div class="row">
            	<div class="col-xs-12 col-md-3">
            		<p style="text-align: center"><label for="sexo">Sexo</label></p>
            	</div>
				<div class="col-xs-12 col-md-9">
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<br>
							<input type="radio" name="sexo" value="F"> Femenino<br>

    						<input type="radio" name="sexo" value="M"> Masculino<br>
						</div>
					</div>
                	
            	</div>
            </div>
		</div>

		<div class="form-group">
		    
			
    		<div class="row">
            	<div class="col-xs-12 col-md-3">
            		<p style="text-align: center"><label for="contenido">Areas</label></p>
            	</div>
				<div class="col-xs-12 col-md-9">
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<select class="form-control" name="area_id">
							    @foreach($areas as $item)
							      <option value="{{$item->id}}">{{$item->nombre}}</option>
							    @endforeach
							</select>	
						</div>
					</div>
                	
            	</div>
            </div>
		</div>

		<div class="form-group">
		    
			
    		<div class="row">
            	<div class="col-xs-12 col-md-3">
            		<p style="text-align: center"><label for="descripcion">Descripcion</label></p>
            	</div>
				<div class="col-xs-12 col-md-9">
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<textarea class="form-control" name="descripcion" rows="3" id="descripcion"> </textarea>

						</div>
					</div>
                	
            	</div>
            </div>
		</div>


		<div clas = "form-group">        
            
            <div class="row">
            	<div class="col-xs-12 col-md-3">
            		<p style="text-align: center">Roles:</p>
            	</div>
				<div class="col-xs-12 col-md-9">
					<div class="row">
						<div class="col-xs-12 col-md-12">
							@foreach ($roles as $id => $nombre)                                         		
							<label>
                        		<input type="checkbox" name="arrayrol[]" value="{{ $id }}">
                        		{{$nombre->nombre}}
                     		</label>
                    		<br>
                			@endforeach
						</div>
					</div>
                	
            	</div>
            </div>
        </div>


		<div class="form-group">
		    
    		<div class="row">
            	<div class="col-xs-12 col-md-3">
            		<p style="text-align: center"><label for="descripcion">Boletin</label></p>
            	</div>
				<div class="col-xs-12 col-md-9">
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<select name="boletin" id="boletin" class="form-control">
							  <option value="1">Si</option>
							  <option value="0">No</option>
							</select>
						</div>
					</div>
                	
            	</div>
            </div>
		</div>

	

	
		<input type="submit" value="ENVIAR" class="btn btn-primary">

 
