@extends('layouts.master')

<!--ponerle titulo a la paginga-->
@section('title', 'Empleados')

@section('content')

    <a class="btn btn-success btn-sm mt-3 mb-3 float-right" href="{{route('crearFormularioPost')}}">Crear Post</a>


    <table class="table table-hover">
        <caption>Empleados</caption>
        <thead>
            <tr>
                <td>Id<td>
                <td>Nombre </td>
                <td>Email</td>
                <td>Sexo</td>
                <td>Area</td>
                <td>Boletin</td>
                <td>Descripcion</td>
                <td>Acciones</td>
            </tr>
        </thead>
        <tbody>
            @foreach ($empleados as $empleado)
            <tr>
                <td>{{ $empleado->id}}</td>
                <td>{{ $empleado->nombre}}</td>
                <td>{{ $empleado->email}}</td>
                <td>{{ $empleado->sexo}}</td>
                <td>{{ $empleado->area_id}}</td>
                <td>{{ $empleado->boletin}}</td>
                <td>{{ $empleado->descripcion}}</td>                
                <td>
                    <div class="row">
                        <div class="col-xs-12 col-md-6 form-group">
                            <a href=" # " class="btn btn-primary">ACT</a>
                            
                        </div>
                    </div>
                        @csrf
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    {{ $empleados->links() }}
    

@endsection