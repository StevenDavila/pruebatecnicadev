@extends('layouts.master')

@section('content')

	@include('vistasParciales.validacionErrores')

	<form action="{{ route("storeFormularioPost")}}" method="POST"> 
		@include('posts._formulario')
	</form>
@endsection