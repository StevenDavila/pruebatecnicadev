<?php

namespace App\Http\Controllers;

use App\Post;
use App\Empleados;
use App\Areas;
use App\Roles;
use DB;
use App\Empleado_rol;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StorePostRequest;
use Illuminate\Support\Facades\View;

class PostController extends Controller
{
    
    public function indexFormularioPost(){
        
        $empleados = Empleados::orderBy('created_at', 'desc')->paginate(10);
        //dd($empleados);
        return view('posts.index',compact('empleados'));
    }

    public function storeFormularioPost(StorePostRequest $request){
        
        $idempleado = Empleados::create([
            'nombre'      =>  $request['nombre'],
            'email'       =>  $request['email'],
            'sexo'        =>  $request['sexo'],
            'area_id'     =>  $request['area_id'],
            'descripcion' =>  $request['descripcion'],
            'boletin'     =>  $request['boletin'],
        ]);

        $rolesarray   =   $request['arrayrol'];
            //dd($perfil);
        foreach ($rolesarray as $id => $rol) {
            $validar = DB::select("select * from empleado_rol where empleado_id = '".$idempleado->id."' 
                    and rol_id = '".$rol."' ");
                
            if($validar == null){
                Empleado_rol::create([
                    'empleado_id' => $idempleado->id,
                    'rol_id'      => $rol,         
                ]);
            }
        }
  
        return back()->with('status', 'Empleado guardado exitosamente!');
      }

    public function crearFormularioPost(){
        
        $areas = Areas::all(['id', 'nombre']);
        $roles = Roles::orderBy('created_at', 'desc')->get();
        return view("posts.create",compact('areas','roles'), ['post' => New Post]);
    }
    
}
