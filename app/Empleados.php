<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleados extends Model
{
    //
    protected $table = 'empleados';

    protected $primarykey = 'id';
    
    protected $fillable = [
        'nombre',
        'email',
        'sexo',
        'area_id',
        'boletin',
        'descripcion',  
    ];

    public function area()
    {
        return $this->hasOne(Areas::class, 'id');
    }
}
