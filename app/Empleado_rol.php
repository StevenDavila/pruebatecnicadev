<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleado_rol extends Model
{
    //
     protected $table = 'empleado_rol';

    protected $primarykey = 'id';
    
    protected $fillable = [
        'empleado_id',
        'rol_id',
    ];
}
